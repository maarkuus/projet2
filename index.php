<?php

include_once './includes/parts/head.php';
include_once './includes/search-header.php';
$titre = "Voilà les commentaires";
$utilisateurs = [
    ["nom" => "Waganwheel", "prenom" => "Jam", "comment" => "c'est hat les tableaux"],
    ["nom" => "Wagaewheel", "prenom" => "Jem", "comment" => "c'est het les tableaux"],
    ["nom" => "Waginwheel", "prenom" => "Jim", "comment" => "c'est hit les tableaux"],
    ["nom" => "Wagonwheel", "prenom" => "Jom", "comment" => "c'est hot les tableaux"],
    ["nom" => "Wagunwheel", "prenom" => "Jum", "comment" => "c'est hut les tableaux"],
    ["nom" => "Wagynwheel", "prenom" => "Jym", "comment" => "c'est hyt les tableaux"]
];

?>

<section class="hero">
    <div class="hero-body">
        <div class="container is-fluid">
            <h1 class="title"><?php echo $titre ?></h1>
        </div>
    </div>

</section>
<section>
    <div class="container is-fluid">

        <?php
        // for ($i = 0; $i < count($utilisateurs); $i++) {
        foreach ($utilisateurs as $cle => $user) {
            echo $cle;
        ?>

            <!-- <h5 class="subtitle is-5">Nom complet: <?php echo $utilisateurs[$i]["prenom"]; ?> <?php echo $utilisateurs[$i]["nom"]; ?></h5>
                <h6 class="subtitle is-6">Commentaire:</h6> -->
            <h5 class="subtitle is-5">Nom complet: <?php echo $user["prenom"]; ?> <?php echo $user["nom"]; ?></h5>
            <h6 class="subtitle is-6">Commentaire:</h6>

            <section>
                <!-- <?php echo $utilisateurs[$i]["comment"]; ?> -->
                <?php echo $user["comment"]; ?>
            </section>
        <?php
        }
        ?>
    </div>
</section>

<?php include_once './includes/parts/footer.php'; ?>