<?php
include_once './includes/functions/contact-functions.php';
include_once './includes/parts/head.php';
include_once './includes/search-header.php';

if (isset($_POST["nom"])) {
    $nom = $_POST["nom"];
}

?>

<section>
    <h1 class="title is-1">Contactez-nous!</h1>
    <div class="container is-fluid">
        <form action="/contact.php" method="POST">
            <input placeholder="Nom" type="text" name="nom" value="<?php form_values("nom") ?>"><br>
            <input placeholder="Prénom" type="text" name="prenom" value="<?php form_values("prenom") ?>"><br>
            <input placeholder="Courriel" type="email" name="courriel" value="<?php form_values("courriel") ?>"> <br>
            <textarea placeholder=" Commentaires" name="contact" id="contact" cols="30" rows="10"></textarea><br>
            <input type="submit" value="Soumettre">
        </form>

    </div>
</section>

<?php
include_once './includes/parts/footer.php';
